package org.gcube.resources.discovery.icclient.stubs;

import static org.gcube.resources.discovery.icclient.stubs.CollectorConstants.portType;
import static org.gcube.resources.discovery.icclient.stubs.CollectorConstants.target_namespace;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;
import jakarta.xml.ws.soap.SOAPFaultException;


/**
 * A local interface to the resource discovery service.
 * 
 * 
 */
@WebService(name=portType,targetNamespace=target_namespace)
public interface CollectorStub {

	/**
	 * Executes a {@link QueryStub}.
	 * @param query the query
	 * @return the query results
	 * @throws MalformedQueryException if the query is malformed
	 * @throws SOAPFaultException if the query cannot be executed
	 */
	@WebMethod(operationName="XQueryExecute")
	@WebResult(name="Dataset")
	String execute(@WebParam(name="XQueryExpression") String query) throws MalformedQueryException;
}
