This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for ic-client

## [v2.0.0]

- Porting to Secret Manager

